"__author__ = 'Leo Chan'"
"__credits__ = 'Keboola 2017'"

"""
Python 3 environment (unicode script fixes in place)
"""
import pip 
pip.main(['install', '--disable-pip-version-check', '--no-cache-dir', 'https://github.com/hay/xml2json/zipball/master'])
pip.main(['install', '--disable-pip-version-check', '--no-cache-dir', 'dateparser'])
pip.main(['install', '--disable-pip-version-check', '--no-cache-dir', 'pygelf'])

import os
import sys
import datetime
import dateparser
import xml2json 
import json
import io
import xml.etree.ElementTree as ET
import optparse
import pandas as pd
import urllib
import ftplib
import logging
import csv
from keboola import docker
from pygelf import GelfTcpHandler

# Environment setup
"""
abspath = os.path.abspath(__file__)
script_path = os.path.dirname(abspath)
os.chdir(script_path)
"""

# initialize application
cfg = docker.Config('/data/')

# access the supplied parameters
params = cfg.get_parameters()
FTP_URL = cfg.get_parameters()["ftp_link"]
CLIENT = cfg.get_parameters()["client"]
USER = cfg.get_parameters()["user"]
PASSWORD = cfg.get_parameters()["#password"]
START_DATE = cfg.get_parameters()["start_date"]
END_DATE = cfg.get_parameters()["end_date"]
DEBUG = cfg.get_parameters()["debug"]

FTP_LINK = "ftp://"+str(USER)+":"+str(PASSWORD)+"@"+str(FTP_URL)+"/"+str(CLIENT)+"/"

DEFAULT_TABLE_FOLDER = "/data/out/tables/"
#out_filename = "wagamama.csv"
out_filename = CLIENT+".csv"
out_destination = DEFAULT_TABLE_FOLDER+out_filename


# Logging
logger = logging.getLogger()
fields = {"_some": {"structured": "data"}}
if DEBUG==True:
    logging.basicConfig(
        level=logging.DEBUG,
        format='%(asctime)s - %(levelname)s - %(message)s',
        datefmt="%Y-%m-%d %H:%M:%S"
        )
    """
    logger.addHandler(GelfTcpHandler(
        host=os.getenv('KBC_LOGGER_ADDR'),
        port=os.getenv('KBC_LOGGER_PORT'),
        debug=True, **fields
        ))
    """
    
else:
    logging.basicConfig(
        level=logging.INFO,
        format='%(asctime)s - %(levelname)s - %(message)s',
        datefmt="%Y-%m-%d %H:%M:%S"
        )
    """
    logger.addHandler(GelfTcpHandler(
        host=os.getenv('KBC_LOGGER_ADDR'),
        port=os.getenv('KBC_LOGGER_PORT'),
        debug=False, **fields
        ))
    """


def dates_request(start_date, end_date):
    dates = []
    try:
        start_date_form = dateparser.parse(start_date)
        end_date_form = dateparser.parse(end_date)
        day_diff = (end_date_form-start_date_form).days
        if day_diff < 0:
            print("ERROR: start_date cannot exceed end_date.")
            print("Exit.")
            sys.exit(0)
        temp_date = start_date_form
        day_n = 0
        if day_diff == 0:
            ### normal with leading zeros
            dates.append(temp_date.strftime("%Y-%m-%d"))
            ### no leading zeros
            #dates.append(temp_date.strftime("%Y/%-m/%-d"))
        while day_n < day_diff:
            ### normal with leading zeros 
            dates.append(temp_date.strftime("%Y-%m-%d"))
            ### no leading zeros
            #dates.append(temp_date.strftime("%Y/%-m/%-d"))
            temp_date += datetime.timedelta(days=1)
            day_n += 1
            if day_n == day_diff:
                ### normal with leading zeros
                dates.append(temp_date.strftime("%Y-%m-%d"))
                ### no leading zeros
                #dates.append(temp_date.strftime("%Y/%-m/%-d"))
    except TypeError:
        print("ERROR: Please enter valid date parameters")
        print("Exit.")
        sys.exit(1)

    return dates


def produce_manifest(file_name):
    """
    Dummy function to return header per file type.
    """

    file = "/data/out/tables/"+str(file_name)+".manifest"
    destination_part = file_name.split(".csv")[0]

    manifest_template = {#"source": "myfile.csv"
                         #,"destination": "in.c-mybucket.table"
                         "incremental": True
                         ,"primary_key": ["VisitID","Value","MenuItem","Section"]
                         ,"columns": [""]
                         #,"delimiter": "|"
                         #,"enclosure": ""
                        }

    column_header = ["VisitID","BranchCode","VisitDate","VisitEntryTime","VisitExitTime","PeriodName","PeriodBranchVisitNumber","ReceiptNumber","Type","Value","Answer","Score","Weight","ExcludeFromVisitScore","HiddenQuestion","Category","QuestionnaireCategory","Tag","Section","Department","Benchmark","MenuItem","TotalSpend","Discarded","SectionNumber","QuestionNumber","CategoryPartition","LinkToVisitReport","Comments"]

    manifest = manifest_template
    manifest["columns"] = column_header
    #manifest["source"] = str(file_name)
    manifest["destination"] = "in.c-hospitalityGEM."+str(destination_part)

    try:
        with open(file, 'w') as file_out:
            json.dump(manifest, file_out)
            logging.info("Output manifest file produced.")
    except Exception as e:
        logging.error("Could not produce output file manifest.")
        logging.error(e)
    print(manifest)
    #return manifest


def save_json(data, file_out):
    """
    Save JSON data as JSON file
    """

    filename = str(file_out)+".json"

    with open(filename, 'w') as jsonfile:
        try:
            json.dump(data, jsonfile)
        except:
            logging.error("Could not output json file.")
            pass

    logging.info(str(file_out)+" file saved.")
    jsonfile.close()


if __name__ == "__main__":
    """
    Main execution script.
    """
    DEFAULT_TABLE_FOLDER = "/data/out/tables/"
    #start_date = "2017-06-26"
    #end_date = "2017-06-27"
    #dates = dates_request(start_date,end_date)
    dates = dates_request(START_DATE,END_DATE)
    header = ["VisitID","BranchCode","VisitDate","VisitEntryTime","VisitExitTime","PeriodName","PeriodBranchVisitNumber","ReceiptNumber","Type","Value","Answer","Score","Weight","ExcludeFromVisitScore","HiddenQuestion","Category","QuestionnaireCategory","Tag","Section","Department","Benchmark","MenuItem","TotalSpend","Discarded","SectionNumber","QuestionNumber","CategoryPartition","LinkToVisitReport","Comments"]

    #file_name = 'WagamamaDataFeed '# 2017-06-26.xml'
    file_name = CLIENT.title()+"DataFeed "
    file_name_none = 'WagamamaDataFeed 2017-03-10.xml' ###doesnt exist###
    
    df = pd.DataFrame(columns=header)

    for i in dates:
        file_name_new = file_name+str(i)
        file_link = FTP_LINK + file_name_new+".xml"
        logging.info("Fetching {0}.xml".format(file_name_new))
    
        try:
            ### Fetching data via urllib ###
            req = urllib.request.Request(file_link)
            logging.info("FTP server connected.")
            response = urllib.request.urlopen(req)
            something = response.read()
            logging.info("Downloading {0}.xml".format(file_name_new))
            body = io.BytesIO(something)

            ### Converting XML to JSON
            tree = ET.parse(body)
            options = None
            options = optparse.Values({"pretty": True})
            tree_json = xml2json.elem2json(tree, options)
            stuff=json.loads(tree_json)
            #print(stuff)

            #############################
            ### OPTION 1:
            ### Output json
            #save_json(stuff,DEFAULT_TABLE_FOLDER+str(i))

            #############################
            ### OPTION 2:
            ### Output CSV
            logging.info("Parsing data into CSV.")
            temp_df = pd.DataFrame(stuff["DATA"]["row"], columns=header)
            df = df.append(temp_df)
            
        except Exception as err:
            logging.warning("ALERT: Unable to fetch {0}.xml - {1}".format(file_name_new, err))

    ### Combining all the dataframe and output as one csv
    logging.info("Exporting CSV.")
    df.to_csv(out_destination, index=False, encoding='utf-8', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    produce_manifest(out_filename)

    logging.info("Script Finish.")